public class Point {
    public double x;
    public double y;
    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    @Override
    public boolean equals(Object other){
        if(other instanceof Point){
            Point newOther = (Point)other;
            return this.x == newOther.x && this.y == newOther.y;
        }else{
            return false;
        }
    }
}
