import java.util.ArrayList;
import java.util.List;
public class ListPractice{
    public static void main(String[] args){
        String[] stringArray = {"WOW","Test","APP"};
        List<String> words = new ArrayList<String>();
        for(String s : stringArray){
            words.add(s);
        }
        System.out.println(words.contains("WOW"));
        System.out.println(words.contains("Sus"));

        System.out.println(getUpperCase(words));
        }
    public static int countUpperCase(String[] strings){
        int counter = 0;
        for(String slot : strings){
            if(slot.matches("^[A-Z]+$") == true){
                counter++;
            }
        }
        return counter;
    }
    public static int countUpperCase(List<String> words){
        int counter = 0;
        for(String slot : words){
            if(slot.matches("^[A-Z]+$") == true){
                counter++;
            }
        }
        return counter;
    }
    public static String[] getUpperCase(String[] strings){
        int counter = 0;
        for(String s : strings){
            if(s.matches("^[A-Z]+$") == true){
                counter++;
            }
        }
        String[] newArray = new String[counter];
        int index = 0;
        for(String s : strings){
            if(s.matches("^[A-Z]+$") == true){
                newArray[index] = s;
                index++;
            }
        }
        return newArray;
    }
    public static List<String> getUpperCase(List<String>words){
        List<String> newWords = new ArrayList<String>();
        for(String s : words){
            if(s.matches("^[A-Z]+$") == true){
            newWords.add(s);
            }
        }
        return newWords;
    }
}