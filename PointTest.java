import java.util.ArrayList;
import java.util.List;
public class PointTest {
    public static void main(String[] args){
        List<Point> points = new ArrayList<Point>();
        Point p1 = new Point(3,5);
        Point p2 = new Point(7,20);
        Point p3 = new Point(6,5);
        points.add(p1);
        points.add(p2);
        points.add(p3);
        Point p4 = new Point(3,5);
        Point target  = p4;
        System.out.println(points.contains(target));
    }
}
